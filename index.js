const express = require('express')
const app = express()
const port = 3000

app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.get('/home', (request, response) => {
	response.send('Welcome to the homepage')
})

//get
let items = [
	{
		"username": "John Doe",
		"password": "johndoe123"
	}
];

app.get('/users', (request, response) => {
	response.send(`${items[0]}`)
})







let users = []; // Mock database

app.get('/register', (request, response) => {

	// Validation process
	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body)
		
		response.send(users)
	} else {
		response.send("Please input BOTH username and password >:(")
	}
})


//delete
app.delete('/delete-user', (request, response) => {
	response.send(`User ${request.body.username} has been deleted`)
})


//listen
app.listen(port, () => console.log(`Server is running at localhost: ${port}`))
